package com.example.dicerollingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //@TODO: Fill in your code here
    public void rollDiceButtonPressed(View view) {
        // TEST THAT YOUR FUNCTION WORKS!!!
        Log.d("JENELLE", "Person clicked on the button");

        // 1. Generate a random number between 1-6
        Random random = new Random();
        int result = random.nextInt(6) + 1;
        Log.d("JENELLE", "Dice rolled: " + result);


        // 2. Update the image based on the number rolled
        ImageView diceImage = (ImageView) findViewById(R.id.imageViewDice);

        switch (result) {
            case 1:
                diceImage.setImageResource(R.drawable.dice1);
                break;
            case 2:
                diceImage.setImageResource(R.drawable.dice2);
                break;
            case 3:
                diceImage.setImageResource(R.drawable.dice3);
                break;
            case 4:
                diceImage.setImageResource(R.drawable.dice4);
                break;
            case 5:
                diceImage.setImageResource(R.drawable.dice5);
                break;
            case 6:
                diceImage.setImageResource(R.drawable.dice6);
                break;
            default:
                diceImage.setImageResource(R.drawable.dice);
                break;
        }
    }
}
